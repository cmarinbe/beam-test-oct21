from Moore import options

options.simulation = False
options.input_type = "MDF"

options.evt_max = 1000

# don't use this for now
#options.use_iosvc = False
#options.event_store = 'EvtStoreSvc'

# tags from JF
conddb_tag = "upgrade/jmarchan-newCaloDecoding" # from JF

# set CONDDB path, where SIMCOND repo lives
# commits in gitlab get deployed to cvmfs so all this is not needed
import os
os.environ['GITCONDDBPATH'] = '/home/cmarinbe/git'

# set CondDB tag
from Configurables import CondDB
CondDB().addLayer('/home/cmarinbe/git/SIMCOND')
#CondDB().Tags['SIMCOND'] = conddb_tag

options.conddb_tag = conddb_tag
options.dddb_tag = "upgrade/master" # from Rosen 
#dddb-20201211" # from JF
