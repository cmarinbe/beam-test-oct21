#include "TFile.h"
#include "TString.h"
#include "TCanvas.h"
#include "TH2.h"

void make_histos(TString fname){

gStyle->SetOptStat(0);
TFile* _file0 = TFile::Open(fname);
TH2D* h_ecal_2D = (TH2D *) _file0->Get("CaloFutureMoniDst/DigitMonitorEcalClusters/8");
TH2D* h_hcal_2D = (TH2D *) _file0->Get("CaloFutureMoniDst/DigitMonitorHcalClusters/8");
TCanvas c1 = new TCanvas();
h_ecal_2D->Draw("colz");
c1.SaveAs("ecal_2D.png");
h_hcal_2D->Draw("colz");
c1.SaveAs("hcal_2D.png");
c1.Close();
}

