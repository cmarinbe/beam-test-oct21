###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.standalone import reco_prefilters
from RecoConf.calorimeter_reconstruction import make_digits
from PyConf.Algorithms import CaloFutureRawToDigits

from PyConf.application import default_raw_event
from PyConf.tonic import bound_parameters

from RecoConf.calo_data_monitoring import monitor_calo_digits, monitor_calo_time_alignment

# save output file with histograms
#options.histo_file = 'calo_monitoring.root'


def hacked_default_raw_event(bank_types=[], raw_event_format=None, maker=None):
    tmp, default_raw_event._substitute = default_raw_event._substitute, None
    if bank_types[0] in ('EcalPacked', 'HcalPacked'):
        maker = bound_parameters(default_raw_event)['maker']
        dh = maker("DAQ/RawEvent", bank_type="Calo")
    else:
        dh = default_raw_event(bank_types, raw_event_format, maker)
    default_raw_event._substitute = tmp
    return dh


def calo_moni():
    rawEventEcal = default_raw_event(["EcalPacked"])
    rawEventHcal = default_raw_event(["HcalPacked"])
    digitsEcal = make_digits(rawEventEcal)["digitsEcal"]
    digitsHcal = make_digits(rawEventHcal)["digitsHcal"]
    calo = {
        "digitsEcal": digitsEcal,
        "digitsHcal": digitsHcal,
    }
    data = monitor_calo_digits(
        calo,
        adcFilterEcal=-100,
        adcFilterHcal=-100,
        HistoMultiplicityMaxEcal=6000,
        HistoMultiplicityMaxHCal=4000,
        spectrum=True)
    data += monitor_calo_time_alignment(
        calo, adcFilterEcal=-100, adcFilterHcal=-100)
    return Reconstruction('calo_moni', data, reco_prefilters(gec=False))


with CaloFutureRawToDigits.bind(IsOldRuns=False,UseParamsFromDB=False):  # needed for new data
    run_reconstruction(options, calo_moni)
