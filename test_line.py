from Moore import options, run_moore
from Hlt2Conf.lines.monitoring.pi0_line import monitoring_pi0_line
# from Hlt2Conf.lines.monitoring.pi0_builders_thor import filter_photons, make_pi0
from RecoConf.global_tools import stateProvider_with_simplified_geom
from GaudiKernel.SystemOfUnits import GeV

def all_lines():
    # with filter_photons.bind(pt_min=0.2 * GeV, p_min=0.5 * GeV):
    #     tight_line = monitoring_pi0_line() 
    # return [tight_line ]
    return [monitoring_pi0_line() ]
    

public_tools = [stateProvider_with_simplified_geom()]

options.set_input_and_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 2000
options.histo_file = 'monitoring_pi0_histo.root'

run_moore(options, all_lines, public_tools)