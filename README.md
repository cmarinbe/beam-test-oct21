# beam-test-oct21

Steps to be able to run these scripts on MDF data with calo raw banks:

### Connect to the plus machines (you need an online account for this):
  - from the cern network: `ssh -tt -XY user@lbgw ssh -XY plus`
  - from outside: `ssh -tt -XY user@lxplus.cern.ch ssh -tt -XY lbgw ssh -XY plus`
It is recommended to create a bash alias for these commands for practical reasons.

### Get a Moore dev folder and checkout the relevant branches:
Do this in the same folder where you have cloned the `beam-test-oc21` repository (not inside it):
```
lb-dev --nightly -c x86_64_v2-centos7-gcc10-opt Moore/HEAD
cd MooreDev_HEAD
git lb-use LHCb
git lb-checkout LHCb/calo_testbeam CaloFuture/CaloFutureDAQ
git lb-use Rec
git lb-checkout Rec/calo_testbeam CaloFuture/CaloFutureMoniDst
git lb-use Moore
git lb-checkout Moore/calo_testbeam Hlt/RecoConf
make
```

### Clone SIMCOND repo locally, in a different folder:
```bash
git clone ssh://git@gitlab.cern.ch:7999/lhcb-conddb/SIMCOND.git
```
If this doesn't work due to the permissions, you probably don't have an ssh-key for this machine uploaded to git.
Alternatively, you can get a kereberos ticket for your cern user and then clone SIMCOND through https:
```bash
kinit username@CERN.CH
git clone https://gitlab.cern.ch/lhcb-conddb/SIMCOND.git
```
where `username` should be replaced by your CERN user name, and `CERN.CH` should be in capital letters.

### Adapt to your local paths
change the paths under `input_mdf.py` to point to the directory where you just cloned SIMCOND

### Change the input file
The runs are saved under `/hlt2/objects/CALO` followed by the run number. Any file under the run number directory contains a subset of events. You can pick up any of them (or all of them).
You can copy them to the daq area for a faster I/O: `/daqarea1/lhcb/data20211026/`

### Run
Run from the MooreDev_HEAD folder:
```
./run gaudirun.py ../beam-test-oct21/conds_mdf.py ../beam-test-oct21/input_test.py ../beam-test-oct21/calo_moni.py | tee test_calo_moni.log
```
- the conditions (tags) and number of events to run are set in `conds_mdf.py`, modify them if needed.
- the input files are defined in `input_test.py`. One can modify this script or write a new one and call that instead to run on different files.
- the decoding and monitoring is configured in `calo_moni.py`. It shouldn't be necessary to edit this file.

Once the job is completed, it should produce an output root file named `calo_monitoring.root` with `DigitMonitorEcalClusters` and `DigitMonitorHcalClusters` folders. The 2D histograms labelled "8" are the most informative ones (especially when using the "colz" option).

## Quick histogram save
A script is provided to extract the most relevant plots easily, just do:
```
./run root -lbq "make_histos.cpp(\"calo_monitoring.root\")" 

TH2D* h_ecal_2D = (TH2D *) _file0->Get("CaloFutureMoniDst/DigitMonitorEcalClusters/8")
TH2D* h_hcal_2D = (TH2D *) _file0->Get("CaloFutureMoniDst/DigitMonitorHcalClusters/8")
TCanvas c1 = new TCanvas()
h_ecal_2D->Draw("colz")
c1.SaveAs("ecal_2D.png")
h_hcal_2D->Draw("colz")
c1.SaveAs("hcal_2D.png")
```

## Decoding fix
The ECAL decoding in `LHCb/master` does not deal properly with missing/faulty fibers. A fix is needed to avoid saturated cells around the faulty ones. A temporal patch has been added to `LHCb/calo_testbeam`, you can check it out in your Moore_Dev project
(Note: you only need this if you hadn't checked out already the LHCb/calo_testbeam branch when setting up your `MooreDev_HEAD` folder):
```bash
git lb-use LHCb
git lb-checkout LHCb/calo_testbeam CaloFuture/CaloFutureDAQ
```
remember to make before running again.

## Thresdholds

### ADC threshold
The recommended ADC thresdhold is around 240 ADCs and is needed to remove the noise.

### Energy threshold (better disabled)
An ET threshold of `6300. * MeV` has been tried but given that the gains are not calibrated it's better to avoid cuts in ET.

## Analyse multiple files in parallel
A Makefile named `Makefile_runCalo` is provided to easily run several Moore jobs in parallel (one per MDF file).
The instructions below assume you run from :
- log in to one of the machines (hceb01, hceb03, hceb04) and check how many cores are available with `htop`
- open a screen on the machine with `screen`
- activate LbEnv to be able to run Moore with `source setup.sh`
- check that the files to analyse are in the folder /daqarea1/lhcb/data2021{MMDD}/{run_number} or copy them there
- check that there are two folders inside `beam-test-oct21/` called `histos` and `logs` or create them
- To point to the input files use make with -e datayyyymmdd=<datayyyymmdd> run_number=<run_number> or input_folder=<input_folder>
  - Alternatively you can create a symbolic link in `/daqarea1/lhcb/data20211029/` labelled `latest` pointing to the folder of the run number where the files are stored with `ln -s latest {run_number place}` or change the makefile.
- check that the makefile is well configured with `make -f Makefile_runCalo -n -e datayyyymmdd=<datayyyymmdd> run_number=<run_number>`
- finally to execute 30 jobs in parallel run:
```
make -f Makefile_runCalo -e datayyyymmdd=<datayyyymmdd> run_number=<run_number> -j30
```
the makefile needs to be executed from within the `beam-test-oct21` folder and it assumes you have a folder names `MooreDev_HEAD` with the Moore setup. The output root files will be stored in the `/histos` folder.

To merge the output files in a single root run:
```
../run hadd Run_{run_number}_merged.root Run_{run_number}*
```

